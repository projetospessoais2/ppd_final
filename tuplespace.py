import linsimpy

tse = linsimpy.TupleSpaceEnvironment()

tse.out(("USERS", tuple([])))

lastMessages = []

def criaUsuario(userName): 
    tse.out(("USER", userName))

    envUsers = tse.inp(("USERS", object))
    envUsers = list(envUsers[1])
    envUsers.append(userName)
    tse.out(("USERS", tuple(envUsers)))

    return f"Usuário: {userName} criado"

def existe(nome):
    try:
        tupla = tse.rdp(("USER", nome))
        return 1
    except:
        return 0

def listUsers():
    arrayListUsers = []
    users = tse.rdp(("USERS", object))
    users = list(users[1])
    for user in users:
        arrayListUsers.append(str(user))

    return arrayListUsers

def enviamsg(p1,p2,msg):
    tse.out((p1,p2,msg))

def recebeMsg(p1,p2,msg):
    resp = tse.inp((p1,p2,msg))
    return f"{str(resp[1])} recebendo msg de {str(resp[0])}: {str(resp[2])}"