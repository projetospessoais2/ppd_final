import threading
import grpc

import proto.chat_pb2 as chat
import proto.chat_pb2_grpc as rpc

import connectMQTT as mqtt_connection

mqttClient = mqtt_connection.connectMqtt()
mqttClient.loop_start()

# def __listen_for_messages():
#         """
#         Este método será executado em um encadeamento separado como o encadeamento principal/ui, porque a chamada for-in está bloqueando
#          ao aguardar novas mensagens
#         """
#         for note in conn.ChatStream(chat.Empty()):  # esta linha irá aguardar novas mensagens do servidor!
#             print("R[{}] {}".format(note.name, note.message))  # declaração de depuração

def getMqttMessages():
    return mqtt_connection.getAllMessages()

def send_message(topic, message):
    """
    Este método é chamado quando o usuário digita algo na caixa de texto
    """
    if message != '':
        n = chat.Note()  # criar mensagem protobuf (chamada Nota)
        n.name = topic  # definir o nome de usuário
        n.message = message  # definir a mensagem real da nota
        conn.SendNote(n)  # enviar a nota para o servidor
        # mqtt_connection.subscribeTopic(mqttClient, topic)
        mqtt_connection.publish(mqttClient, message, topic)


address = 'localhost'
port = 11912

channel = grpc.insecure_channel(address + ':' + str(port))
conn = rpc.ChatServerStub(channel)
# crie um novo thread de escuta para quando novos fluxos de mensagens chegarem
# threading.Thread(target=__listen_for_messages, daemon=True).start()