from flask import Flask, render_template, request, redirect
import tuplespace as ts
import connectRPC as rpc_connection

app = Flask(__name__)

palavrasSuspeitas = []
mensagensChat     = []

@app.route('/', methods=['GET', 'POST'])
def index():
    cadastrouUsuario = 0
    if request.method == "POST":
        username = request.form.get('username')
        username = str(username).upper()
        if (ts.existe(username) == 0):
            ts.criaUsuario(username)
            cadastrouUsuario = 1
        else:
            cadastrouUsuario = 2

    try:
        listUsers = ts.listUsers()
    except:
        listUsers = []

    return render_template('index.html', cadastrouUsuario=cadastrouUsuario, listUsers=listUsers)

@app.route('/chat', methods=['GET', 'POST'])
def chat():
    alertMessage = 0
    if request.method == "POST":
        message  = request.form.get('message')
        userFrom = request.form.get('user_from')
        userTo   = request.form.get('user_to')
        if (userFrom == userTo):
            alertMessage = 1
        elif (message == ''):
            alertMessage = 2
        else:
            observe = verificaSuspeita(message)
            if (observe[1] == 1): 
                messageToMqtt = f"{str(userTo)} recebendo msg de {str(userFrom)}: {str(message)}"
                rpc_connection.send_message(observe[0], messageToMqtt)

            ts.enviamsg(userFrom, userTo, message)
            mensagensChat.append(ts.recebeMsg(userFrom, userTo, message))
    try:
        listUsers = ts.listUsers()
    except:
        listUsers = []

    return render_template('chat.html', listUsers=listUsers, alertMessage=alertMessage, mensagensChat=mensagensChat)

@app.route('/monitor', methods=['GET', 'POST'])
def indexMonitor():
    
    listMessages = rpc_connection.getMqttMessages()

    splitedArrayMessage = []
    for msg in listMessages:
        splitted = msg.split('] ')
        splitedArrayMessage.append([splitted[0][1:], splitted[1]])

    return render_template('index_adm.html', listMessages=splitedArrayMessage)
    # return 'Hello, World!'

@app.route('/addsuspeita', methods=['GET', 'POST'])
def addMsgSuspeita():
    showMessage = False
    if request.method == "POST":
        message = request.form.get('message')
        palavrasSuspeitas.append(message)
        showMessage = True
    return render_template('add_suspeita.html', palavrasSuspeitas = palavrasSuspeitas, showMessage = showMessage)

def verificaSuspeita(msg):
    foundSuspect = 0
    suspectWord = ''
    for palavraSuspeita in palavrasSuspeitas:
        if palavraSuspeita.upper() in msg or palavraSuspeita.lower() in msg:
            suspectWord = palavraSuspeita
            foundSuspect = 1
    return [suspectWord, foundSuspect]