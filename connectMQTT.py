import paho.mqtt.client as paho
import random

broker = 'broker.emqx.io'
port = 1883
client_id = f'python-mqtt-{random.randint(0, 1000)}'

arrayMessages = []

def connectMqtt():
        def on_connect(client, userdata, flags, rc):
            # client.subscribe('Geral')
            if rc == 0:
                print("Conectado ao MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        def on_message(client, userdata, msg):  # The callback for when a PUBLISH =
            payloadMsg = msg.payload.decode()
            payloadMessage = "[{}] {}".format(msg.topic, str(payloadMsg))
            arrayMessages.append(payloadMessage)

        client = paho.Client(client_id)
        client.on_connect = on_connect
        client.on_message = on_message
        client.user_data_set([])
        client.connect(broker, port)
        return client

def publish(client, msg, topic):
        client.subscribe(topic)
        result = client.publish(topic, msg)

def getAllMessages():
    return arrayMessages
        